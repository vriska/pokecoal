#include "global.h"
#include "main.h"
#include "menu.h"
#include "graphics.h"
#include "bg.h"
#include "malloc.h"
#include "palette.h"
#include "pokedex_area_region_map.h"

static EWRAM_DATA u8 *sPokedexAreaMapBgNum = NULL;

void LoadPokedexAreaMapGfx(const struct PokedexAreaMapTemplate *template)
{
    void * tilemap;
    sPokedexAreaMapBgNum = Alloc(sizeof(sPokedexAreaMapBgNum));

    SetBgAttribute(template->bg, BG_ATTR_METRIC, 0);
    DecompressAndCopyTileDataToVram(template->bg, gRegionMapBg_GfxLZ, 0, template->offset, 0);
    tilemap = DecompressAndCopyTileDataToVram(template->bg, gRegionMapBg_TilemapLZ, 0, 0, 1);
    AddValToTilemapBuffer(tilemap, template->offset, 32, 32, FALSE); // template->offset is always 0, so this does nothing.

    ChangeBgX(template->bg, 0, BG_COORD_SET);
    ChangeBgY(template->bg, 0, BG_COORD_SET);
    SetBgAttribute(template->bg, BG_ATTR_PALETTEMODE, 1);
    memcpy(&gPlttBufferUnfaded[BG_PLTT_ID(7)], gRegionMapBg_Pal, 48 * 2);
    *sPokedexAreaMapBgNum = template->bg;
}

bool32 TryShowPokedexAreaMap(void)
{
    if (!FreeTempTileDataBuffersIfPossible())
    {
        ShowBg(*sPokedexAreaMapBgNum);
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

void FreePokedexAreaMapBgNum(void)
{
    TRY_FREE_AND_SET_NULL(sPokedexAreaMapBgNum);
}

void PokedexAreaMapChangeBgY(u32 move)
{
    ChangeBgY(*sPokedexAreaMapBgNum, move * 0x100, BG_COORD_SET);
}
