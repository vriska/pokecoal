	.align 2
voicegroup276:: @ B2W2 Sato
	voice_keysplit voicegroupInst1, KeySplitTable90	@ voice_keysplit voicegroup323, KeySplitTable133   @ piano (missing C4)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup237, KeySplitTable9     @ rhodes 2 (hgss)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup235, KeySplitTable91    @ piano octave
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup294, KeySplitTable36    @ rhodes
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup300, KeySplitTable36    @ rhodes (short release)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup234, KeySplitTable36    @ piano 2
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup290, KeySplitTable70    @ harpsichord
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup323, KeySplitTable59    @ piano (all splits)
	voice_directsound 60, 0, DirectSoundWaveData_dp_glocken_c6_16, 255, 247, 0, 208
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup297, KeySplitTable124   @ vibraphone || 80
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup297, KeySplitTable76    @ vibraphone
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup295, KeySplitTable74    @ marimba
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup240, KeySplitTable39    @ xylophone
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_228vibrc6, 255, 248, 0, 205
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup241, KeySplitTable25    @ tubular bell
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup243, KeySplitTable76    @ music box
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup244, KeySplitTable71    @ drawbar organ
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup245, KeySplitTable71    @ jazz organ
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup291, KeySplitTable71    @ accordion
	voice_keysplit voicegroupInst15, KeySplitTable87	@ nylon guitar
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup250, KeySplitTable32    @ distortion guitar 2 (hgss)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_168slapbc2, 255, 197, 227, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup342, KeySplitTable41    @ jazz guitar
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup316, KeySplitTable41    @ clean guitar
	voice_keysplit voicegroup252, KeySplitTable42    @ upright bass
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup253, KeySplitTable43    @ fingered bass
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup311, KeySplitTable86    @ fretless bass
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup309, KeySplitTable92    @ slap bass
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup254, KeySplitTable73    @ synth bass
	voice_keysplit voicegroup255, KeySplitTable45    @ strings
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup298, KeySplitTable82    @ strings 2
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup256, KeySplitTable46    @ violin
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup257, KeySplitTable66    @ cello
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup293, KeySplitTable48    @ pizzicato
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup299, KeySplitTable78    @ harp 2
	voice_keysplit voicegroup259, KeySplitTable40	@ voice_keysplit voicegroup302, KeySplitTable80    @ harp 1
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup319, KeySplitTable97    @ pipe organ
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup260, KeySplitTable61    @ timpani
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup261, KeySplitTable39    @ trumpets
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup262, KeySplitTable47    @ trombones
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup264, KeySplitTable50    @ french horns (hgss)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup327, KeySplitTable41    @ french horns
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup305, KeySplitTable44    @ tuba
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup288, KeySplitTable46    @ oboe
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup266, KeySplitTable52    @ clarinet
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup267, KeySplitTable53    @ flute 1 (hgss)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup266, KeySplitTable113   @ clarinet | 60
	voice_keysplit voicegroup267, KeySplitTable53	@ voice_keysplit voicegroup328, KeySplitTable65    @ flute 1
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup268, KeySplitTable54    @ bassoon
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup314, KeySplitTable71    @ alto sax
	voice_directsound 60, 0, DirectSoundWaveData_sc88pro_wind, 67, 243, 155, 224
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup260, KeySplitTable131   @ timpani || 64
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup327, KeySplitTable72    @ french horns | 52 | 60
	voice_directsound 60, 0, DirectSoundWaveData_hg_096flt1c6, 255, 246, 78, 192
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_bell_ringing, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown1, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown2, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown3, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown4, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown5, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_unknown93, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup303, KeySplitTable81    @ choir full
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup344, KeySplitTable121   @ synth vox
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_wind_grotto, 255, 197, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_areyouready, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_areyouready_high, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_ahh, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_casteliagymvox1, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_casteliagymvox2, 255, 0, 255, 32
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup330, KeySplitTable108   @ sitar
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_steeldrumc4_undella, 255, 251, 0, 205
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_bassdrum_ranch, 255, 239, 0, 232
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_cowbells, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_cowbellsloop, 255, 0, 255, 0
	voice_keysplit_all voicegroup337				 @ drum set 2
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit_all voicegroup336				 @ drum set 1
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit_all voicegroup338				 @ drum set 3 (addictive drums)
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_pinkstrate2, 255, 253, 0, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_solotrumpetc5, 255, 0, 255, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_trombonesstaccatocs2, 255, 42, 227, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_bw_spiccatostringse2, 255, 0, 255, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_trumpetsstaccatogs4_loop, 255, 0, 255, 224
	voice_directsound 60, 0, DirectSoundWaveData_hg_021cellog2, 254, 242, 255, 205
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup343, KeySplitTable65    @ pan flute
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup352, KeySplitTable128   @ gamelan
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup340, KeySplitTable118   @ pink strat
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_12oclock, 255, 251, 14, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_iris, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup347, KeySplitTable123   @ square lead
	voice_directsound 60, 0, DirectSoundWaveData_dp_sawtoothlead60, 255, 197, 255, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_doublsawc5, 255, 197, 255, 171
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup350, KeySplitTable123   @ dj handy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup351, KeySplitTable127   @ spanish guitar
	voice_directsound 60, 0, DirectSoundWaveData_puresquare_50, 255, 242, 25, 0
	voice_square_2 60, 0, 2, 0, 7, 2, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit voicegroup348, KeySplitTable126   @ over run
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_vinylsfx, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_sewers, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_casteliagym, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_beautyintro, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_colress, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_keysplit_all voicegroup339				 @ contains join avenue drum loop chops
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_beauty, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_driftveilgym, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_pwtjohtoleader, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_pwtjohtochamp, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_pwtjohtochamp_phaser, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_pwtsinnohleader, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_cheren, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_reversalmountain, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_shadowtriad, 255, 0, 255, 0
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_drumloop_humilaugym, 255, 0, 255, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0                          @ dummy
	    voice_square_1 60, 0, 0, 2, 0, 0, 15, 0 @ voice_directsound 60, 0, DirectSoundWaveData_b2_cry_tympole, 255, 0, 255, 0
