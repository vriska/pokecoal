	.align 2
voicegroup337:: @ B2W2 Drum Set 2
	voice_directsound 60, 64, DirectSoundWaveData_dp_bassdr1_16, 255, 0, 255, 171					@ 127/127/127/120/ 64
	voice_directsound 60, 64, DirectSoundWaveData_dp_oct_snare_16, 255, 0, 255, 0					@ 127/127/127/127/ 64
	voice_directsound 60, 64, DirectSoundWaveData_bw_171snare909, 255, 0, 227, 205    				@ 127/127/120/116/ 64	
	voice_directsound 59, 64, DirectSoundWaveData_hg_032crash, 255, 242, 0, 214						@ 127/100/  0/114/ 64	- 1
	voice_directsound 60, 110, DirectSoundWaveData_dp_closed_hihat_16, 255, 0, 227, 32				@ 127/127/120/125/110
	voice_directsound 60, 110, DirectSoundWaveData_dp_open_hihat_16, 255, 241, 0, 0					@ 127/102/  0/127/110
	voice_directsound 60, 90, DirectSoundWaveData_b2_chh_colress, 255, 84, 0, 205					@ 127/124/  0/116/ 90
	voice_directsound 60, 64, DirectSoundWaveData_b2_drumloop_pwthoennleader1, 255, 0, 255, 0		@ 127/127/127/127/ 64
	voice_directsound 60, 64, DirectSoundWaveData_b2_drumloop_pwthoennleader2, 255, 0, 255, 0		@ 127/127/127/127/ 64
	voice_directsound 60, 36, DirectSoundWaveData_hg_032crash, 255, 248, 0, 214						@ 127/ 80/  0/114/ 36
	voice_directsound 60, 90, DirectSoundWaveData_b2_ohh_colress, 255, 241, 227, 205				@ 127/102/  0/116/ 90
	voice_directsound 60, 100, DirectSoundWaveData_b2_chh_colress, 255, 0, 255, 192					@ 127/127/127/118/100
	voice_directsound 60, 100, DirectSoundWaveData_b2_ohh_colress, 255, 0, 255, 171					@ 127/127/127/120/100
	voice_directsound 57, 64, DirectSoundWaveData_dp_cymbal1_16, 255, 246, 0, 214					@ 127/ 90/  0/114/ 64	- 3
	voice_directsound 29, 30, DirectSoundWaveData_dp_tom1_16, 255, 0, 255, 32						@ 127/127/127/125/ 30	-12
	voice_directsound 34, 40, DirectSoundWaveData_dp_tom1_16, 255, 0, 255, 32						@ 127/127/127/125/ 40	- 7
	voice_directsound 59, 20, DirectSoundWaveData_dp_china_cynbal, 255, 250, 0, 226					@ 127/ 70/  0/110/ 20	- 1
	voice_directsound 60, 64, DirectSoundWaveData_dp_synthsnare, 255, 0, 255, 0						@ 127/127/127/127/ 64
	voice_directsound 60, 80, DirectSoundWaveData_hg_062chh, 255, 242, 0, 171						@ 127/100/  0/120/ 80
	voice_directsound 62, 20, DirectSoundWaveData_dp_china_cynbal, 255, 250, 0, 226					@ 127/ 70/  0/110/ 20	+ 2
	voice_directsound 63, 20, DirectSoundWaveData_dp_china_cynbal, 255, 250, 0, 226					@ 127/ 70/  0/110/ 20	+ 3
	voice_directsound 60, 36, DirectSoundWaveData_dp_cymbal1_16, 255, 250, 0, 216					@ 127/ 70/  0/114/ 36
	voice_directsound 61, 102, DirectSoundWaveData_dp_cymbal1_16, 255, 250, 0, 216					@ 127/ 70/  0/114/102	+ 1
	voice_directsound 60, 64, DirectSoundWaveData_bw_108kick909, 255, 0, 227, 205    				@ 127/127/120/116/ 64
	voice_directsound 60, 64, DirectSoundWaveData_b2_synthkick, 255, 0, 227, 171    				@ 127/127/120/120/ 64	
	voice_directsound 61, 64, DirectSoundWaveData_b2_synthkick, 255, 0, 227, 171    				@ 127/127/120/120/ 64	+ 1
	voice_directsound 48, 64, DirectSoundWaveData_dp_reverscyn_16, 2, 0, 227, 171			    	@   3/127/120/120/ 64
	voice_directsound 60, 64, DirectSoundWaveData_dp_053clap, 255, 197, 227, 205					@ 127/120/120/116/ 64
	voice_directsound 61, 64, DirectSoundWaveData_dp_053clap, 255, 197, 227, 205					@ 127/120/120/116/ 64	+ 1
	voice_directsound 60, 64, DirectSoundWaveData_bw_159rim, 255, 0, 227, 205    					@ 127/127/120/116/ 64	
	voice_directsound 62, 64, DirectSoundWaveData_dp_053clap, 255, 197, 227, 205					@ 127/120/120/116/ 64	+ 2
	voice_directsound 60, 64, DirectSoundWaveData_dp_sne2_loop, 255, 0, 227, 205    				@ 127/127/120/116/ 64
	voice_directsound 60, 64, DirectSoundWaveData_bw_175snare, 255, 0, 227, 205    					@ 127/127/120/116/ 64	
	voice_directsound 60, 64, DirectSoundWaveData_bw_010kick, 255, 0, 227, 205    					@ 127/127/120/116/ 64
	voice_directsound 60, 64, DirectSoundWaveData_bw_011kick, 255, 0, 227, 205    					@ 127/127/120/116/ 64
	voice_directsound 60, 64, DirectSoundWaveData_bw_110kick, 255, 0, 227, 205						@ 127/127/120/116/ 64
	voice_directsound 60, 64, DirectSoundWaveData_bw_109kick, 255, 0, 227, 205    					@ 127/127/120/116/ 64
	voice_directsound 62, 64, DirectSoundWaveData_bw_110kick, 255, 0, 227, 205						@ 127/127/120/116/ 64	+ 2
	voice_directsound 60, 64, DirectSoundWaveData_bw_176snare, 255, 0, 227, 205						@ 127/127/120/116/ 64
	voice_directsound 61, 64, DirectSoundWaveData_bw_172snare909, 255, 0, 227, 205    				@ 127/127/120/116/ 64	+ 1
	voice_directsound 60, 64, DirectSoundWaveData_dp_093snare, 255, 226, 127, 205    				@ 127/114/ 90/116/ 64
	voice_directsound 60, 30, DirectSoundWaveData_hg_135tom, 255, 0, 255, 205    					@ 127/127/127/116/ 30	
	voice_directsound 60, 80, DirectSoundWaveData_hg_062chh, 255, 84, 0, 205						@ 127/124/  0/116/ 80
	voice_directsound 61, 64, DirectSoundWaveData_dp_093snare, 255, 226, 127, 205    				@ 127/114/ 90/116/ 64	+ 1
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 80, DirectSoundWaveData_hg_063ohh, 255, 241, 0, 205						@ 127/102/  0/116/ 80
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 64, DirectSoundWaveData_hg_033orccym, 255, 250, 0, 214					@ 127/ 70/  0/114/ 64
	voice_directsound 60, 64, DirectSoundWaveData_hg_032crash, 255, 246, 0, 214						@ 127/ 90/  0/114/ 64
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 62, 64, DirectSoundWaveData_hg_032crash, 255, 246, 0, 214						@ 127/ 90/  0/114/ 64	+ 2
	voice_directsound 60, 20, DirectSoundWaveData_hg_022china, 255, 248, 0, 224						@ 127/ 80/  0/110/ 20	
	voice_directsound 60, 30, DirectSoundWaveData_dp_ridecap, 255, 251, 0, 210						@ 127/ 60/  0/115/ 30
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 84, DirectSoundWaveData_bw_179splash, 255, 250, 0, 205    				@ 127/ 70/  0/116/ 84
	voice_directsound 60, 110, DirectSoundWaveData_hg_031cowbl, 255, 0, 255, 205					@ 127/127/127/116/110
	voice_directsound 60, 40, DirectSoundWaveData_bw_043crash, 255, 250, 0, 205    					@ 127/ 62/  0/116/ 40
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 80, DirectSoundWaveData_hg_032crash, 255, 246, 0, 214						@ 127/ 90/  0/114/ 80
	voice_directsound 60, 64, DirectSoundWaveData_bw_118conga, 255, 0, 255, 205    					@ 127/127/127/116/ 64
	voice_directsound 60, 20, DirectSoundWaveData_bw_118conga, 255, 0, 255, 205    					@ 127/127/127/116/ 20
	voice_directsound 60, 64, DirectSoundWaveData_dp_060conga, 255, 0, 227, 205    					@ 127/127/120/116/ 64
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 58, 30, DirectSoundWaveData_dp_060conga, 255, 0, 227, 205    					@ 127/127/120/116/ 30	- 2
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 105, DirectSoundWaveData_bw_118conga, 255, 0, 255, 205    				@ 127/127/127/116/105
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 63, 64, DirectSoundWaveData_dp_060conga, 255, 0, 227, 205    					@ 127/127/120/116/ 64	+ 3
	voice_directsound 60, 64, DirectSoundWaveData_b2_synthkick_pwtsinnohleader, 255, 0, 227, 171    @ 127/127/120/120/ 64	
	voice_directsound 61, 64, DirectSoundWaveData_b2_synthkick_pwtsinnohleader, 255, 0, 227, 171    @ 127/127/120/120/ 64	+ 1
	voice_directsound 66, 30, DirectSoundWaveData_dp_060conga, 255, 0, 227, 205    					@ 127/127/120/116/ 30	+ 6
	voice_directsound 60, 40, DirectSoundWaveData_b2_guirochop1, 255, 234, 191, 205    				@ 127/110/110/116/ 40
	voice_directsound 60, 40, DirectSoundWaveData_b2_guirochop2, 255, 234, 191, 205    				@ 127/110/110/116/ 40
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 40, DirectSoundWaveData_bw_084guiro_short, 255, 234, 191, 205    			@ 127/110/110/116/ 40
	voice_directsound 60, 40, DirectSoundWaveData_bw_083guiro_long, 255, 234, 191, 205    			@ 127/110/110/116/ 40
	voice_directsound 60, 25, DirectSoundWaveData_dp_062sleigh, 255, 224, 227, 205    				@ 127/115/120/116/ 25
	voice_directsound 60, 30, DirectSoundWaveData_bw_201tamb, 255, 224, 227, 205    				@ 127/115/120/116/ 30	
	voice_directsound 60, 40, DirectSoundWaveData_bw_windchime, 255, 234, 191, 205    				@ 127/110/110/116/ 40
	voice_directsound 60, 32, DirectSoundWaveData_bw_213tri_closed, 255, 234, 191, 205    			@ 127/110/110/116/ 32
	voice_directsound 60, 32, DirectSoundWaveData_bw_214tri_open, 255, 234, 191, 205    			@ 127/110/110/116/ 32
	voice_directsound 60, 30, DirectSoundWaveData_b2_woodblockhi, 255, 0, 255, 0    				@ 127/127/127/127/ 30
	voice_directsound 60, 100, DirectSoundWaveData_b2_woodblocklo, 255, 0, 255, 0    				@ 127/127/127/127/100
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 64, DirectSoundWaveData_bw_gongp, 255, 0, 255, 0    						@ 127/127/127/127/ 64
	voice_directsound 60, 64, DirectSoundWaveData_b2_synthsnare_shadowtriad, 255, 0, 227, 205    	@ 127/127/120/116/ 64	
	voice_directsound 60, 30, DirectSoundWaveData_b2_ohh_shadowtriad, 255, 251, 0, 210    			@ 127/ 60/  0/115/ 30	
	voice_directsound 63, 80, DirectSoundWaveData_bw_101chh909, 255, 239, 0, 205    				@ 127/105/  0/116/ 80	+ 3
	voice_directsound 61, 80, DirectSoundWaveData_bw_104ohh909, 255, 241, 0, 205    				@ 127/102/  0/116/ 80	+ 1
	voice_directsound 60, 58, DirectSoundWaveData_dp_tambourine_16, 255, 0, 255, 205				@ 127/127/127/116/ 58
	voice_directsound 60, 46, DirectSoundWaveData_bw_synthchh_vsrival, 255, 0, 255, 0    			@ 127/127/127/127/ 46
	voice_directsound 60, 46, DirectSoundWaveData_bw_synthohh_vsrival, 255, 246, 0, 0    			@ 127/ 90/  0/127/ 46
	voice_directsound 60, 64, DirectSoundWaveData_dp_bassdr2_16, 255, 0, 255, 0						@ 127/127/127/127/ 64
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 60, 64, DirectSoundWaveData_dp_093snare, 255, 226, 127, 0    					@ 127/114/ 90/127/ 64
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_directsound 62, 64, DirectSoundWaveData_dp_093snare, 255, 226, 127, 0    					@ 127/114/ 90/127/ 64	+ 2
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
	voice_square_1 60, 0, 0, 2, 0, 0, 15, 0
