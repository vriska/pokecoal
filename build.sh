#!/usr/bin/env bash
set -e
cd "$(dirname "$0")"
if ! [[ -f tools/poryscript/poryscript ]]; then
    rm -rf tools/poryscript
    wget https://github.com/huderlem/poryscript/releases/download/3.4.0/poryscript-linux.zip
    unzip -jd tools/poryscript poryscript-linux.zip
    rm poryscript-linux.zip
fi
docker build . -t pokecoal
docker run --rm -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro -u "$(id -u):$(id -g)" -v $PWD:$PWD -w $PWD pokecoal make -j"$(nproc)" modern DINFO=1
