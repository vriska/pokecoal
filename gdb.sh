#!/usr/bin/env bash
set -e
cd "$(dirname "$0")"
if ! [[ -f tools/poryscript/poryscript ]]; then
    rm -rf tools/poryscript
    wget https://github.com/huderlem/poryscript/releases/download/3.0.2/poryscript-linux.zip
    unzip -jd tools/poryscript poryscript-linux.zip
    rm poryscript-linux.zip
fi
docker build . -t pokecoal
docker run --net host --rm -it -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro -u "$(id -u):$(id -g)" -v $PWD:$PWD -w $PWD pokecoal /opt/devkitpro/devkitARM/bin/arm-none-eabi-gdb pokecoal.elf -ex 'target extended-remote localhost:2345'
