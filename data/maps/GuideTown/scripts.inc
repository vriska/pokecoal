GuideTown_MapScripts::
# 2 "data/maps/GuideTown/scripts.pory"
	map_script MAP_SCRIPT_ON_TRANSITION, GuideTown_MapScripts_MAP_SCRIPT_ON_TRANSITION
	.byte 0

GuideTown_MapScripts_MAP_SCRIPT_ON_TRANSITION:
# 3 "data/maps/GuideTown/scripts.pory"
	getplayerxy VAR_TEMP_0, VAR_TEMP_1
# 4 "data/maps/GuideTown/scripts.pory"
	compare VAR_TEMP_0, 20
	goto_if_lt GuideTown_MapScripts_MAP_SCRIPT_ON_TRANSITION_1
# 4 "data/maps/GuideTown/scripts.pory"
	compare VAR_TEMP_0, 32767
	goto_if_gt GuideTown_MapScripts_MAP_SCRIPT_ON_TRANSITION_1
	return

GuideTown_MapScripts_MAP_SCRIPT_ON_TRANSITION_1:
# 5 "data/maps/GuideTown/scripts.pory"
	clearflag FLAG_HIDE_GUIDE_TOWN_TREE
	return


GuideTown_EventScript_PlayersHouseSign::
# 11 "data/maps/GuideTown/scripts.pory"
	lockall
# 12 "data/maps/GuideTown/scripts.pory"
	msgbox GuideTown_EventScript_PlayersHouseSign_Text_0
# 13 "data/maps/GuideTown/scripts.pory"
	releaseall
	end


GuideTown_EventScript_TwainsHouseSign::
# 18 "data/maps/GuideTown/scripts.pory"
	lockall
# 19 "data/maps/GuideTown/scripts.pory"
	msgbox GuideTown_EventScript_TwainsHouseSign_Text_0
# 20 "data/maps/GuideTown/scripts.pory"
	releaseall
	end


GuideTown_EventScript_SaphronethsLabSign::
# 25 "data/maps/GuideTown/scripts.pory"
	lockall
# 26 "data/maps/GuideTown/scripts.pory"
	msgbox GuideTown_EventScript_SaphronethsLabSign_Text_0
# 27 "data/maps/GuideTown/scripts.pory"
	releaseall
	end


GuideTown_EventScript_PlayersHouseSign_Text_0:
# 12 "data/maps/GuideTown/scripts.pory"
	.string "{PLAYER} and {RIVAL}'s House$"

GuideTown_EventScript_TwainsHouseSign_Text_0:
# 19 "data/maps/GuideTown/scripts.pory"
	.string "Twain's House$"

GuideTown_EventScript_SaphronethsLabSign_Text_0:
# 26 "data/maps/GuideTown/scripts.pory"
	.string "Saphroneth's Lab$"
