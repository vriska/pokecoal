Route15_MapScripts::
	map_script MAP_SCRIPT_ON_TRANSITION, Route15_MapScripts_MAP_SCRIPT_ON_TRANSITION
	.byte 0

Route15_MapScripts_MAP_SCRIPT_ON_TRANSITION:
	getplayerxy VAR_TEMP_0, VAR_TEMP_1
	compare VAR_TEMP_0, -1
	goto_if_ne Route15_MapScripts_MAP_SCRIPT_ON_TRANSITION_1
	return

Route15_MapScripts_MAP_SCRIPT_ON_TRANSITION_1:
	clearflag FLAG_HIDE_GUIDE_TOWN_TREE
	return

